import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import Create from './Create'
import Edit from './Edit'
import Product from './Product';

class Index extends Component{
  constructor(props) {
    super(props);
    this.state = {
        data: [],
        dataEdit: {},
        loading: true, 
        edit:false,
        currentPage: 1,
        todosPerPage: 3
    };

    this.index = this.index.bind(this);
    this.updateProducts = this.updateProducts.bind(this);
    this.updateProduct = this.updateProduct.bind(this);
    this.deleteProduct = this.deleteProduct.bind(this);
    this.editProduct = this.editProduct.bind(this);

  }

  componentDidMount(){
    this.index();
  }
  
  index(){
    this.setState({
      loading:true,
      edit:false
    })
    let storedArray = JSON.parse(sessionStorage.getItem("items"));
    if(storedArray !== null){
      this.setState({
        data: storedArray, 
        loading: false,
      });
    }
    this.setState({ loading: false })
    console.log(this.state.data)
  }

  updateProducts(newData){
    let storedArray = [...this.state.data, newData];
    window.sessionStorage.setItem("items", JSON.stringify(storedArray));
    this.index();
  }

  deleteProduct(id) {
    //obtener copia del state
    const productosActuales = this.state.data;

    //borrar el elemento del state
    const codigos = productosActuales.filter(codigos => codigos.id !== id );

    //Actualizar el sistema
    window.sessionStorage.setItem("items", JSON.stringify(codigos));
    this.index();
  }

  updateProduct(data) {
    //obtener copia del state
    const productosActuales = this.state.data;
    let newData = [];
    productosActuales.forEach((i) => {
      
      if(data.id === i.id){
        console.log('iguales')
        newData.push({
          key:data.id,
          id:data.id, 
          nombre:data.nombre, 
          codigo:data.codigo, 
          tipo:data.tipo, 
          caducidad:data.caducidad,
        })
      }else{
        newData.push(i)
      }
    });
    window.sessionStorage.setItem("items", JSON.stringify(newData));
    this.index();
  }

  editProduct(data){
    this.setState({edit: true, dataEdit: data})
  }

  handleClick(event) {
    this.setState({
      currentPage: Number(event.target.id)
    });
  }

  render() {
    if(this.state.loading){
      return(
        <div>Cargando</div>
      )
    }else{
      let storedArray = JSON.parse(sessionStorage.getItem("items"));
      let data = [];
      if(storedArray !== null){
        data = storedArray;
      }
      const { currentPage, todosPerPage } = this.state;
      console.log(data);
      return (
          <div>
          <Grid container spacing={3}>
            <Grid item xs={6}>
              {!this.state.edit ? (
                <Create updateProducts={this.updateProducts} data={this.dataEdit}/>
              ) : (
                <Edit
                updateProduct={this.updateProduct}
                id={this.state.dataEdit.id} 
                nombre={this.state.dataEdit.nombre} 
                codigo={this.state.dataEdit.codigo} 
                tipo={this.state.dataEdit.tipo} 
                caducidad={this.state.dataEdit.caducidad}/>
              )}

            </Grid>

            <Grid item xs={6}>
             {data.length > 0 ? data.map(i => (
               <Product 
                  key={i.id}
                  id={i.id} 
                  nombre={i.nombre} 
                  codigo={i.codigo} 
                  tipo={i.tipo} 
                  caducidad={i.caducidad}
                  deleteProduct={this.deleteProduct}
                  editProduct={this.editProduct}
               />
             )): (
               <div>No hay productos</div>
             )}
            </Grid>
          </Grid>
        </div>
      );
    }
  }
}

export default Index;
