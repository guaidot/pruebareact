import React, { Component } from 'react';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

class Product extends Component{
    constructor(props) {
        super(props);
        this.handleEdit = this.handleEdit.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    handleEdit(){
        this.props.editProduct(this.props)
    }
    handleDelete(){
        this.props.deleteProduct(this.props.id)
    }
   
  render() {
    return (
        <div>
        <Card>
            <CardContent>
                <Typography variant="h5" component="h2">
                    Producto: {this.props.nombre}
                </Typography>
            </CardContent>
            <CardContent>
                <ul>
                    <li>Id: {this.props.id}</li>
                    <li>Codigo: {this.props.codigo}</li>
                    <li>Tipo: {this.props.tipo}</li>
                    <li>Caducidad: {this.props.caducidad}</li>
                </ul>
            </CardContent>

            <CardActions>
                <Button
                variant="contained"
                color="primary"
                size="small"
                onClick={this.handleEdit}>Editar</Button>
                
                <Button
                variant="contained"
                color="secondary"
                size="small"
                onClick={this.handleDelete}>Eliminar</Button>
            </CardActions>
        </Card>
      </div>
    );
  }
}

export default Product;
