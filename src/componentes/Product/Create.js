import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import uuid from 'uuid';
import InputLabel from '@material-ui/core/InputLabel';

class Index extends Component{
    constructor(props) {
        super(props);
        this.state = {
            id: uuid(),
            nombre: '',
            codigo: '',
            tipo: '',
            caducidad: '',
        };
    
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    
    handleChange(e) {
    this.setState({[e.target.name]: e.target.value})
    }

    handleSubmit(event) {
        console.log(this.state);
        for (const key in this.state) {
            if(this.state[key] === ''){
                alert('Todos los campos son requeridos');
                return false;
            }
        }
        this.props.updateProducts(this.state);
        this.setState({
            id: uuid(),
            nombre: '',
            codigo: '',
            tipo: '',
            caducidad: '',
        })
        event.preventDefault();
    }

   
  render() {
    return (
        <div>
        <Card>
            <CardContent>
                <Typography variant="h5" component="h2">
                Crear producto
                </Typography>
            </CardContent>
            <CardContent>

                <div>
                 <form noValidate autoComplete="off">
                        <Grid container spacing={1} alignItems="flex-end">
                        <Grid item xs={12}>
                                <TextField 
                                    value={this.state.nombre} 
                                    name="nombre" 
                                    label="Nombre" 
                                    variant="outlined" 
                                    fullWidth 
                                    onChange={this.handleChange}/>
                            </Grid>
                            <Grid item xs={12}>
                                <TextField 
                                    value={this.state.codigo} 
                                    name="codigo" 
                                    label="Codigo" 
                                    variant="outlined" 
                                    fullWidth 
                                    onChange={this.handleChange}/>
                            </Grid>
                            <Grid item xs={12}>
                                <TextField 
                                    value={this.state.tipo} 
                                    name="tipo" 
                                    label="Tipo" 
                                    variant="outlined" 
                                    fullWidth 
                                    onChange={this.handleChange}/>
                            </Grid>
                            <Grid item xs={12}>
                                <InputLabel htmlFor="input-with-icon-adornment">Caducidad</InputLabel>
                                <TextField 
                                    value={this.state.caducidad} 
                                    type="date" 
                                    name="caducidad" 
                                    variant="outlined" 
                                    fullWidth 
                                    onChange={this.handleChange}/>
                            </Grid>
                        </Grid>
                        <CardActions>
                            <Button
                            variant="contained"
                            color="primary"
                            size="small"
                            onClick={this.handleSubmit}>Guardar</Button>
                        </CardActions>
                    </form>
                </div>

            </CardContent>
        </Card>
      </div>
    );
  }
}

export default Index;
