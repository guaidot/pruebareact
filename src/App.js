import React, { Component } from 'react';
import Header from './componentes/Header';
import Index from './componentes/Product/Index';
import Container from '@material-ui/core/Container';



class App extends Component{
  state = {
    codigos: []
}


  render() {
    return (
      <Container maxWidth="lg">
        <Header titulo={'Productos'} />
        <Index/>
      </Container>
    );
  }
}

export default App;
